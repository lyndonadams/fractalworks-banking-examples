<div align="center">
  <h1>Joule Banking Demo</h1>
</div>
<div align="center">
  <strong>Modern use case platform</strong>
</div>
<div align="center">
  A framework for creating streaming applications
</div>

<div align="center">
  <h3>
    <a href="https://www.fractalworks.io/joule-platform">
      Website
    </a>
    <span> | </span>
    <a href="https://hub.docker.com/r/fractalworks/joule">
      DockerHub
    </a>
    <span> | </span>
    <a href="https://fractalworks.slack.com/archives/C021AM4M142">
      Slack
    </a>
  </h3>
</div>

## Table of Contents
- [Overview](#overview)
  - [Features Demonstrated](#joule-features-demonstrated)
- [Stock Quote Simulator](#stock-quote-simulator)
- [Use cases](#use-cases)
- [Build and Run](#build-and-run)
- [Starting an example](#starting-an-example)
- [Dependency Requirements](#dependency-requirements)
  - [Postgres](#postgres)
  - [Kafka](#kakfa)  
  - [InfluxDB](#influxdb)
- [SQL Tap](#sql-tap)
- [RestAPI](#restapi)
- [Offline analytics](#offline-analytics)
- [Building custom components](#building-custom-components)
- [Support](#support)

## Overview
The objective of this project is to demonstrate Joule real-time analytics capabilities using time and event based window aggregate functions. Six 
use case files have been created that demonstrates various platform capabilities. 

External market data events are generated using a basic simulator based upon a static nasdaq end of day market data file, published on to Kafka quotes topic.
Events are consumed and processed by Joule with resulting analytics published into a InfluxDB time series bucket. Thereafter Grafana visualisations or and other suitable tool can be applied.

### Joule features demonstrated
1. Sliding windows (i.e. time and event based)
2. Group by aggregate functions
3. User defined analytics using SDK (Bollinger Bands)
4. Event filtering
5. Event projection
6. InfluxDB transport
7. Kafka publisher and consumer transports
8. Kafka event transformers
9. SQLTap that captures processed events within stream
10. RestAPI to access raw and processed events

## Stock Quote Simulator
Simulated stock quote events are created using the StockQuoteSimulator driver. A total of 7514 stock quotes are generated and published on each processing cycle. 
Update the StockQuoteSimulator class for additional fields and processing requirements. 
The ```conf/csv/nasdaq.csv``` file used to prime the simulator. 

### Quote Event
The following fields are available on each quote event
```
- ingestTime
- eventTime
- symbol
- bid
- ask
- volume
- volatility
- date
```

The Kafka configuration can be found here ```conf/simulator/kafkapublisher.yaml```.

## Use cases
Currently five use cases are provided within this project, files provided under ```conf/usecases``` directory.

1. Tumbling window analytics using 5 second time buckets, file ```tumblingWindowAnalytics.yaml```
2. Sliding window analytics using 2.5 second time buckets with a 500ms sliding window, file ```slidingWindowAnalytics.yaml```
3. Exponential Moving Average sliding window using a platform pre-defined function, file ```slidingEventWindowEMAAnalytics.yaml```
4. Bollinger bands sliding window analytics using 5 events bucket using the Joule SDK to create a user defined function, file ```slidingEventWindowBollingerBandsAnalytics.yaml```
5. Aggregates and user defined time based window functions file ```slidingEventWindowAnalyticsAggregatesAndEMA.yaml```

All the use case are defined using the Joule low-code DSL. All the use case definition files 
found in the ```conf/usecases``` directory.

#### Example
```yaml
stream:
  name: standardQuoteAnalyticsStream
  enabled: true
  validFrom: 2020-01-01
  validTo: 2025-12-31
  eventTimeType: EVENT_TIME
  sources: [ nasdaq_quotes_stream ]

  processing unit:
    pipeline:
      - time window:
          emitting type: coreQuoteAnalytics
          aggregate functions:
            MIN: [ ask ]
            MAX: [ bid ]
            SUM: [ volume ]
            MEAN: [ volatility ]
          policy:
            type: slidingTime
            slideSize: 500
            windowSize: 2500

  emit:
    select: "symbol, ask_MIN, bid_MAX, volume_SUM, volatility_MEAN"

  group by:
    - symbol
```

### Use case 1
This is a simple example using tumbling windows that demonstrates a set of standard aggregate functions. The resulting data can be published to
either file or influxDB using the ```xxxxxxStandardAnalytics.yaml``` files.

When using the timeWindow processor the only a single event per window process is created on process complete, all previous events are deleted.

### Use case 2
This is a simple example using sliding windows that demonstrates a set of standard aggregate functions. The resulting data can be published to
either file or influxDB using the ```xxxxxxStandardAnalytics.yaml``` files.

### Use case 3
This use case using declarative filter and sliding event window SDK defined function. The event window EMA function is trigger
after 5 events using previous calculation. The function will calculate EMA for bother the ask and bid attributes over the event window.
The resulting data can be published to either file or influxDB ```influxdbEMA.yaml``` files.

### Use case 4
This use case using declarative filter and sliding event window SDK defined function. The event window EMA function is trigger
after 5 events using previous calculation. The resulting data can be published to
either file or influxDB ```influxdbBollingerBands.yaml``` files.

### Use case 5
This use case using declarative filter, sliding window calculations including aggregates and
an example of  ```AbstractStatefulWindowListener``` and ```AbstractStatelessWindowListener``` interfaces implemetations using the Joule SDK.
The resulting data can be published to file ```fileStandardAnalytics.yaml``` files.

If you like to use Grafana to visualise the data first create the Flux query in Influx Explore dashboard. The example Flux query can be used to generate a Grafana dashboard query.
```
from(bucket: "nasdaqBaseAnalytics")
|> range(start: v.timeRangeStart, stop: v.timeRangeStop)
|> filter(fn: (r) => r["_measurement"] == "quote-View")
|> filter(fn: (r) => r["symbol"] == "CVCO")
|> filter(fn: (r) => r["_field"] == "ask_lower_BollingerBand" or r["_field"] == "ask_middle_BollingerBand" or r["_field"] == "ask_upper_BollingerBand")
|> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
|> yield(name: "mean")
```


## Build and Run
This project is bundled with a small set of custom components and therefore will require a local build. Build the project in the root directory by following the below instructions.

First build the project
```bash
gradle clean build && cd build/distributions && unzip fractalworks-banking-example.zip && cd fractalworks-banking-example && chmod +x bin/*.sh
```

By building the example locally you will gain an understanding how to create new use cases, analytics, processors, transports. 
Any contributions back the to project are most welcome.

## Starting an example
First rename the use case ```app.env.XX file``` you like to run to ```app.env``` under the ```conf/bin``` directory.  

```
File: app.env.influxdb
SOURCEFILE=./conf/sources/stockQuoteStream.yaml
ENGINEFILE=./conf/usecases/slidingWindowAnalytics.yaml
PUBLISHFILE=./conf/publishers/influxdbStandardAnalytics.yaml
```

Example
```bash
cp bin/app.env.influx bin/app.env
```

### Running Joule in a container
The Joule image exposes a shared volume under ```/app/joule```. You can mount a host directory to that point to access persisted container data. A typical invocation of the container might be:

``` bash
docker run -d --name=joule_processor \
-p 1098:1098 \
-p 7070:7070 \
-v $PWD/conf:/app/joule/conf \
-v $PWD/userlibs:/app/joule/userlibs \
-v $PWD/logs:/app/joule/logs: \
-v $PWD/data:/app/joule/data: \
-v $PWD/META-INF:/app/joule/META-INF: \
--network joule_procesing_network \
--env-file ./bin/app.env \
fractalworks/joule:latest
```

## Dependency Requirements
Startup Postgres, Confluent Kafka, InfluxDB and Grafana using the provided scripts located under there respective infra directories

### Postgres

#### docker-compose
```bash
docker-compose -f infra/postgres/docker-compose.yml up -d
```

To use Postgres you will need to perform three simple tasks, define a target schema, copy jdbc postgres jar and link the ```databaseStandardAnalytics.yaml```
file in the ```bin/app.env``` file.

#### Step 1
Add latest postgres JDBC driver to ```lib``` directory.
```bash
cp infra/postgres/postgresql-42.5.0.jar lib/
```

#### Step 1
Define and deploy database schema file, example below.
```yaml
CREATE TABLE IF NOT EXISTS quote_analytics (
    id              SERIAL PRIMARY KEY,
    symbol          VARCHAR(10),
    ask_EMA         float8,
    bid_EMA         float8
    );
```

#### Step 3
Modify the provided example database publisher file to your setting, the current configuration will work OOTB. See ```conf/publishers/databaseStandardAnalytics.yaml```
```yaml
publisher:
  name: standardAnalyticsDatabasePublisher
  source: standardQuoteAnalyticsStream
  sinks:
    - sqlPublisher:
        jdbcDriver: org.postgresql.Driver
        host: jdbc:postgresql://localhost:5432
        database: jouleTestDB
        table: quote_analytics
        includeTimestamps: false
        properties:
          ssl: false
          user: joule
          password: joulepwd
```

### Kakfa
Kafka is used as the event stream where quote events are published on to a topic ready for Joule to process. 
The provided ```docker-compose.yml``` files has had a few modifications to enable container-to-container and local-to-container communication using configured network setting. 
Select either Confluent or Redpanda to use a Kafka implementation.

#### Confluent docker-compose
```bash
docker-compose -f infra/confluent/docker-compose.yml up -d
```

#### Redpanda docker-compose
```bash
docker-compose -f infra/redpanda/docker-compose.yml up -d
```

Redpanda has been configured to use a the hostname of KAFKA_BROKER. Please set this up in your client and server ```/etc/hosts``` file

### InfluxDB
We have provided a file that starts up both InfluxDB a Grafana. Follow the instructions below for further configurations.

#### docker-compose
```bash
docker-compose -f infra/influx-grafana/docker-compose.yml up -d
```

The ```infra/influx-grafana/.env``` file contains all the required configuration setting for InfluxDB and Grafana.
Processed events will be stored using the below org and bucket.

* user: influx
* password: influxdb
* organisation: banking
* bucket: nasdaqBaseAnalytics

In the event of changing the setting these need to be replicated in the influxDB publishing ```conf/publishers/influxdbStandardAnalytics.yaml``` file.

### Start Quote generator
Once the infrastructure is running it's time to start running the use case.

Start quote generator
```bash
./bin/startQuotePublisher.sh
```

Start joule processor
```bash
./bin/startJoule.sh
```
Events will be generated, published, consumed, process and finally published to InfluxDB and ready to be visualised in Grafana.

Note: If you are not using the Joule docker image you will need to 
edit the Kafka ```clusterAddress``` to ```localhost:9092``` in the ```conf/sources/stockQuoteStream.yaml```. 


## SQL Tap
Joule has the ability to store stream events to an internal SQL store. This is useful to enable offline analytics over raw events
or perform in stream analytics using custom processors, more on this in a later post and example. 

To capture and store processed events add the below declaration anywhere before or after a processing function. 
The current implementation only supports a fixed table structure at the time of creation i.e. any new fields added to an event 
after the table creation will not be stored. The use case ```tumblingWindowSQLTapAndAnalytics.yaml``` file provides an example of this feature.

```yaml
- tap:
    target schema: nasdaq_quotes
    queue capacity: 2000
    flush frequency: 500
```

One of the new key features Joule provides OOTB raw and final processed event capture. No additional configuration is needed as the platform
dynamically generates the required target table and access queries.

## RestAPI
The Joule process provides two restapi endpoints to access raw and processed event data. The schema data is stored is named using the use case 
name defined within the DSL i.e. ```standardQuoteAnalyticsStream```.

```bash
# Raw
curl -o processed-events.json "http://localhost:7070/joule/standardQuoteAnalyticsStream/raw?offset=2000"

# Processed
curl -o processed-events.json "http://localhost:7070/joule/standardQuoteAnalyticsStream/processed?offset=2000"
```

## Offline analytics
You can perform offline analytics by connecting to the DuckDB database using your IDE of your choice. 
The latest DuckDB JDBC driver need to be used to access the database.

```bash
URL: jdbc:duckdb:<HOME>/fractalworks-banking-example-1.0/db/joule.db
Properties:
  duckdb.read_only=True
```

A further example will discuss how best to leverage this feature.

## Building custom components
Follow the below steps to create a new use case function.

1. Create a function by implementing AnalyticsFunction interface
2. Return types expected are either a Double or Map<String, Double>
3. Test it in isolation using a unit test
4. Add class package and name to ```META-INF/services/plugins.properties``` file
5. Replicate ```slidingEventWindowBollingerBandsAnalytics``` file using your own setting
6. Modify app.env file using your use case file
7. Test using the standard file published file provided.
8. Create a new publishing influxdb file, using ```influxdbBollingerBands.yaml``` file as a template


## Support
Creating examples and a platform takes a significant amount of work. Joule is independently developed, funded and maintained by Fractalworks Ltd. 
If you would like to support please contact [Fractalworks enquiries](mailto:enquiries@fractalworks.io) .Many thanks to Huntabyte who created the reusable docker-compose scripts for InfluxDb and Grafana [Youtube link](https://www.youtube.com/watch?v=QGG_76OmRnA) and Confluent.

#### Misc - Create random API token
If you want to change the API token for InfluxDB use the below command and ensure the the relevant files are updated.
```bash
openssl rand -hex 32
```