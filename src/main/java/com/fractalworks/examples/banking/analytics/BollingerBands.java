/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.examples.banking.analytics;

import com.fractalworks.streams.sdk.analytics.AnalyticsFunction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Bollinger bands function
 *
 * @author Lyndon Adams
 */
public class BollingerBands implements AnalyticsFunction<Map<String, Double>>  {

    private int deviations = 2;
    public BollingerBands(){}

    @Override
    public Map<String, Double> compute(Number[] values, Number previousValue) {

        Double mean = Arrays.stream(values).mapToDouble(d-> (double) d).sum() / values.length;
        double sqrtmean = 0.0;
        for(int i=0; i<values.length; i++){
            double s =  values[i].doubleValue() - mean;
            sqrtmean += s * s;
        }


        sqrtmean = Math.sqrt( sqrtmean / values.length);
        double band = deviations * sqrtmean;

        Map<String, Double> results = new HashMap<>();
        results.put("upper", mean + band);
        results.put("middle", mean );
        results.put("lower", mean - band);
        return results;
    }

    @Override
    public void setParameters(Properties parameters) {
        if( parameters != null && parameters.containsKey("deviations")) {
            deviations = Integer.parseInt(parameters.get("deviations").toString());
        }
    }

    @Override
    public String getVariablePostFixID() {
        return "BollingerBand";
    }
}
