package com.fractalworks.examples.banking.data;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.StreamEventParser;

import java.util.Collection;
import java.util.Collections;

/**
 * Custom domain transformer to convert Quote to a StreamEvent
 *
 * @author Lyndon Adams
 */
public class QuoteToStreamEventParser implements StreamEventParser {

    public QuoteToStreamEventParser() {
        // Required
    }

    @Override
    public Collection<StreamEvent> translate(Object o) throws TranslationException {
        Collection<StreamEvent> events = null;
        if(o instanceof Quote){
            Quote quote = (Quote) o;
            StreamEvent event = new StreamEvent("quote");
            event.setEventTime(quote.getTime());
            event.addValue("symbol", quote.getSymbol());
            event.addValue("bid", quote.getBid());
            event.addValue("ask", quote.getAsk());
            event.addValue("volatility", quote.getVolatility());
            event.addValue("volume", quote.getVolume());
            event.addValue("date", quote.getDate());

            events = Collections.singletonList( event);
        }
        return events;
    }
}
