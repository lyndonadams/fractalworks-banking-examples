package com.fractalworks.examples.banking.data;

import java.util.Objects;

public record StockAnalyticRecord(String symbol, Long time, Double askFirst, Double askLast) {
    public StockAnalyticRecord{
        Objects.requireNonNull(symbol);
        Objects.requireNonNull(time);
        Objects.requireNonNull(askFirst);
        Objects.requireNonNull(askLast);
    }
}
