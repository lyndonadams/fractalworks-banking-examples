package com.fractalworks.examples.banking.data;

import java.io.Serializable;

/**
 * Simple StockInfo record
 *
 * @author Lyndon Adams
 */
public record StockInfo (
    String symbol,
    String name,
    double lastSale,
    double netChange,
    double percentageChange,
    double marketCap,
    String country,
    String ipoYear,
    long volume,
    String sector,
    String industry
) implements Serializable {}
