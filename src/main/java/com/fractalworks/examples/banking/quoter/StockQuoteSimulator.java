/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.examples.banking.quoter;

import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.examples.banking.data.StockInfo;
import com.fractalworks.examples.banking.data.Quote;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.kafka.KafkaPublisherTransport;
import com.fractalworks.streams.transport.kafka.specification.KafkaPublisherSpecification;
import com.fractalworks.streams.transport.kafka.specification.KakfaPublisherSpecificationBuilder;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;

import picocli.CommandLine;
import picocli.CommandLine.Option;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Simple stock quote simulator
 *
 * @author Lyndon Adams
 */
public class StockQuoteSimulator implements Runnable {

    @Option(names = {"-k", "--kafkaspec"}, description = "kafka publishing specification", required = false)
    private String kafkaPublishingSpecification = "conf/simulator/kafkapublisher.yaml";

    @Option(names = {"-q", "--quotefile"}, description = "file containing required symbols to create quotes", required = false)
    private String quoteGenerationFile = "data/csv/nasdaq.csv";

    @Option(names = {"-f", "--frequency"}, description = "delay between quotes", required = false)
    long delayBetweenQuotes = 0; // In milliseconds

    @Option(names = {"-s", "--symbol"}, description = "single symbol to generate quotes", required = false)
    String requiredSymbol;

    Map<String, Tuple<StockInfo, Quote>> stockQuotes = new ConcurrentHashMap<>();

    double maxDeltaChange = 0.50;
    double maxSpread = 0.66;
    int volumeBound = 1500;

    static Random random = new Random();

    LocalDate date = LocalDate.now();

    private static KafkaPublisherTransport publisherTransport;
    private final ScheduledExecutorService scheduledPublishingService = Executors.newScheduledThreadPool(1);

    private final String EVENT_TYPE = "nasdaq";
    private final String SYMBOL = "symbol";
    private final String TIME = "time";
    private final String MID = "mid";
    private final String BID = "bid";
    private final String ASK = "ask";
    private final String VOLUME = "volume";
    private final String VOLATILITY = "volatility";
    private final String DATE = "date";

    public StockQuoteSimulator() {
    }

    public void init() throws URISyntaxException, IOException, CsvValidationException, InvalidSpecificationException, TransportException {
        File f = new File( quoteGenerationFile);
        Reader reader = Files.newBufferedReader(Paths.get( f.toURI() ));

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(',')
                .withIgnoreQuotations(true)
                .build();

        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();

        String[] line;
        while ((line = csvReader.readNext()) != null) {
            var volume = Long.valueOf( (line[8] != null && !line[8].isBlank()) ? line[8] : "12345678");

            StockInfo si = new StockInfo( line[0],
                    line[1],
                    Double.valueOf( (line[2] != null && !line[2].isBlank()) ? line[2] : "0.0"),
                    Double.valueOf( (line[3] != null && !line[3].isBlank()) ? line[3] : "0.0"),
                    Double.valueOf( (line[4] != null && !line[4].isBlank()) ? line[4] : "0.0"),
                    Double.valueOf( (line[5] != null && !line[5].isBlank()) ? line[5] : "0.0"),
                    line[6],
                    line[7],
                    ( volume == 0) ? volume : 12345678,
                    line[9],
                    line[10]
                    );
            stockQuotes.put( si.symbol(), new Tuple<>(si, null));
        }
        reader.close();
        csvReader.close();

        // Setup publisher
        initPublisher();
    }

    /**
     * Setup and initialise Kafka publisher
     *
     * @throws InvalidSpecificationException
     * @throws URISyntaxException
     */
    public void initPublisher() throws InvalidSpecificationException, TransportException{
        KakfaPublisherSpecificationBuilder builder = new KakfaPublisherSpecificationBuilder();
        KafkaPublisherSpecification spec = (KafkaPublisherSpecification) builder.build(  new File(kafkaPublishingSpecification));

        publisherTransport = new KafkaPublisherTransport(spec);
        publisherTransport.initialize();
        scheduledPublishingService.scheduleAtFixedRate(publisherTransport, spec.getSendTimeout(), spec.getSendTimeout(), TimeUnit.MILLISECONDS);
    }

    public void sendQuotes(){
        Collection<StreamEvent> quotes = new ArrayList<>();
        if( requiredSymbol != null ){
            StreamEvent quote = createStockQuote(stockQuotes.get(requiredSymbol));
            quotes.add(quote);
        } else {
            stockQuotes.forEach((s, q) -> {
                StreamEvent quote = createStockQuote(q);
                quotes.add(quote);
            });
        }
        publisherTransport.publish( quotes );
    }

    private StreamEvent createStockQuote(Tuple<StockInfo, Quote>  previousQuote){

        StockInfo si = previousQuote.getX();
        Quote pQ = previousQuote.getY();

        var nextDouble = random.nextDouble();
        var direction =  (nextDouble > 0.5) ? 1 : -1;

        var volume = Math.abs(random.nextInt(volumeBound) % (si.volume() / 420));
        var movement = (nextDouble % maxDeltaChange )  * direction;;
        var volatility = (nextDouble % maxDeltaChange );
        double spread = (random.nextDouble() % maxSpread );
        double mid;
        double bid;
        double ask;
        long time = System.currentTimeMillis();

        if(delayBetweenQuotes > 0 ){
            if( pQ == null ) {
                time = System.currentTimeMillis();
            } else {
                time = pQ.getTime() + (random.nextInt() % delayBetweenQuotes);
            }
        }

        if( pQ == null ){
            mid = si.lastSale();
        } else {
            mid = pQ.getMid();
            mid += movement;
        }

        bid = mid - spread;
        ask = mid + spread;

        // Create quote event
        StreamEvent event = new StreamEvent( EVENT_TYPE, time);
        event.addValue(SYMBOL, si.symbol().intern());
        event.addValue(TIME, time);
        event.addValue(MID, mid);
        event.addValue(BID, Math.abs(bid));
        event.addValue(ASK, Math.abs(ask));
        event.addValue(VOLUME, volume);
        event.addValue(VOLATILITY, volatility);
        event.addValue(DATE, date);

        pQ = new Quote(
                si.symbol().intern(),
                mid,
                bid,
                ask,
                volume,
                volatility,
                time,
                date);

        previousQuote.setY( pQ);
        return event;
    }

    public static void main(String[] args) throws CsvValidationException, URISyntaxException, IOException, InvalidSpecificationException, TransportException {
        StockQuoteSimulator stockQuoteSimulator = new StockQuoteSimulator();

        // Parse commandline args
        new CommandLine(stockQuoteSimulator).parseArgs(args);
        stockQuoteSimulator.init();

        Runtime.getRuntime().addShutdownHook(new CloseDownHook());
        stockQuoteSimulator.sendQuotes();
        Thread t = new Thread( stockQuoteSimulator);
        t.run();
    }

    @Override
    public void run() {
        while(true){
            sendQuotes();
        }
    }

    /**
     * Shutdown hook
     */
    static class CloseDownHook extends Thread {
        public void run() {
            if( publisherTransport!= null) {
                publisherTransport.shutdown();
            }
        }
    }
}
