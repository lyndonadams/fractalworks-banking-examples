#!/bin/bash
#
# Copyright 2020-present FractalWorks Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
source ${PWD}/version.env

export JOULE_HOME=${PWD}
export LIBS=${JOULE_HOME}/lib
export USERLIBS=${JOULE_HOME}/userlibs
export LOG4J_PATH=${JOULE_HOME}/META-INF/simulator/log4j2.xml
export CLASSPATH=$(find ${LIBS} -name '*.jar' | xargs echo | tr ' ' ':'):$(find ${USERLIBS} -name '*.jar' | xargs echo | tr ' ' ':')

mkdir -p ${PWD}/logs
mkdir -p ${PWD}/data

java=java
if test -n "$JAVA_HOME"; then
    java="$JAVA_HOME/bin/java"
fi

KAFKAFILE=${JOULE_HOME}/conf/simulator/kafkapublisher.yaml
QUOTEFILE=${JOULE_HOME}/data/csv/nasdaq.csv
TICK_FREQUENCY=-1
MEM=-Xmx128M

while [ "$1" != "" ]; do
    case $1 in
        -k | --kafkaspec)     shift
                                KAFKAFILE=$1
                                ;;
        -q | --quotefile )    shift
                                QUOTEFILE=$1
                                ;;
        -s | --symbols )     shift
                                SYMBOLS=$1
                                ;;
        -f | --frequency )     shift
                                TICK_FREQUENCY=$1
                                ;;
        -h | --help )         usage
                              exit
                              ;;
        * )                   usage
                              exit 1
    esac
    shift
done

echo "Starting Quotes Simulator v${JOULE_VERSION}"
if [ -z "$SYMBOLS" ]; then
  nohup java -Dlog4j.configuration=file://${LOG4J_PATH} $MEM -XX:+UseG1GC -cp ${LOG4J_PATH}:${CLASSPATH} com.fractalworks.examples.banking.quoter.StockQuoteSimulator -k ${KAFKAFILE} -q ${QUOTEFILE} -f ${TICK_FREQUENCY} &
else
  nohup java -Dlog4j.configuration=file://${LOG4J_PATH} $MEM -XX:+UseG1GC -cp ${LOG4J_PATH}:${CLASSPATH} com.fractalworks.examples.banking.quoter.StockQuoteSimulator -k ${KAFKAFILE} -q ${QUOTEFILE} -s ${SYMBOLS} -f ${TICK_FREQUENCY} &
fi

_pid=$!

echo "$_pid" > ${PWD}/logs/quotes.pid
echo "ProcessId" "$_pid"