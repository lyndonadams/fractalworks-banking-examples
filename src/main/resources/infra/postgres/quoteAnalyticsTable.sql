CREATE TABLE IF NOT EXISTS quote_analytics (
    id              SERIAL PRIMARY KEY,
    symbol          VARCHAR(10),
    ask_EMA         float8,
    bid_EMA         float8
    );
